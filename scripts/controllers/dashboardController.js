angular.module('ethExplorer')
    .controller('dashboardCtrl', function ($rootScope, $scope, $location, $routeParams, $q) {

        var contractAdressList = ["0xE002f7102912fCCA12871D7745789E5eb9d65E58", "0xc55365145A14f719a0D7BceB59d02020098eb982"];

        var abi = [{"constant":true,"inputs":[],"name":"closeTimestampAuction","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_timestamp","type":"uint256"}],"name":"delivery","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"deliveryTimestamp","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_timestamp","type":"uint256"}],"name":"closeAuction","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"bids","outputs":[{"name":"value","type":"uint256"},{"name":"bidder","type":"address"},{"name":"timestamp","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"},{"name":"_timestamp","type":"uint256"}],"name":"bid","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"dueTimestampDelivery","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"pickupTimestamp","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"deliverer","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"size","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_timestamp","type":"uint256"}],"name":"pickup","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"fromLocation","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"toLocation","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"receiver","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"inputs":[{"name":"_fromLocation","type":"string"},{"name":"_toLocation","type":"string"},{"name":"_size","type":"string"},{"name":"_dueTimestampDelivery","type":"uint256"},{"name":"_receiver","type":"address"}],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_fromLocation","type":"string"},{"indexed":false,"name":"_toLocation","type":"string"},{"indexed":false,"name":"_size","type":"string"},{"indexed":false,"name":"_dueTimestampDelivery","type":"uint256"},{"indexed":false,"name":"_receiver","type":"address"}],"name":"AuctionCreated","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_value","type":"uint256"},{"indexed":false,"name":"_bidder","type":"address"},{"indexed":false,"name":"_timestamp","type":"uint256"}],"name":"NewBid","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_highestbid","type":"uint256"},{"indexed":false,"name":"_deliverer","type":"address"}],"name":"AuctionClosed","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_timestamp","type":"uint256"}],"name":"PickUp","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_timestamp","type":"uint256"}],"name":"Delivery","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"previousOwner","type":"address"},{"indexed":true,"name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"}];

        $scope.init=function()
        {
            getChainInfos()
                .then(function(result){

                    $scope.result = result;

                    $scope.blockNum = web3.eth.blockNumber;

                    //updateStats();
                                    
                    $scope.contractList = [];

                    web3.eth.personal.unlockAccount('0x6e4F388f747225bc8d37D9C5555e81BF76A5299d', 'OmniUber2017!?');

                    web3.eth.defaultAccount = web3.eth.coinbase;

                    web3.eth.defaultAccount = web3.eth.accounts[0];

                    var delivrContract = web3.eth.contract(abi);

                    // instantiate by address
                    var contractInstance = delivrContract.at('0xc55365145A14f719a0D7BceB59d02020098eb982');

                    //contractInstance

                    console.log(contractInstance);
                    console.log(contractInstance.fromLocation());

                    if($rootScope.contractList.length == 0) {
                        $rootScope.contractList = [];

                        /*for (var i = 0; i < 7; i++) {
                            var contractItem = 
                            {
                                closeTimestampAuction: Date.now(),
                                deliveryTimestamp: Date.now(),
                                bids: {
                                    value: 10,
                                    bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                    timestamp: Date.now()
                                },
                                dueTimestampDelivery: Date.now(),
                                pickupTimestamp: Date.now(),
                                deliverer: '0xB266d94519C25fa0b6deAEebF074298051560Ae9',
                                owner: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                                size: 'Box',
                                fromCity: 'Berlin',
                                toCity: 'Karlsruhe',
                                fromLocation: 'fragt.nach.vorsicht',
                                toLocation: 'rätsel.rutschen.bedeutete',
                                receiver: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                                currentState: 'Auction Running',
                                orderId: '0xc55365145A14f719a0D7BceB59d02020098eb982',
                                highestBid: 10
                            };

                            $scope.contractList.push(contractItem);
                        }*/

                        $rootScope.contractList.push({
                            closeTimestampAuction: new Date().set,
                            deliveryTimestamp: Date.now(),
                            bids: {
                                value: 'X',
                                bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                timestamp: Date.now()
                            },
                            dueTimestampDelivery: Date.now(),
                            pickupTimestamp: new Date("Wed Nov 15 2017 10:26:47 GMT+0200 (W. Europe Daylight Time)"),
                            deliverer: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                            owner: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                            size: 'Box',
                            fromCity: 'Bonn',
                            toCity: 'Bonn',
                            fromLocation: 'ordering.dragons.grownup',
                            toLocation: 'addicted.bucket.hammer',
                            receiver: '0xB266d94519C25fa0b6deAEebF074298051560Ae9',
                            currentState: 'OPEN',
                            orderId: '0xc55365145A14f719a0D7BceB59d02020098eb982',
                            highestBid: '22'
                        });

                        $rootScope.contractList.push({
                            closeTimestampAuction: new Date().set,
                            deliveryTimestamp: Date.now(),
                            bids: {
                                value: 'X',
                                bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                timestamp: Date.now()
                            },
                            dueTimestampDelivery: Date.now(),
                            pickupTimestamp: new Date("Wed Nov 15 2017 10:26:47 GMT+0200 (W. Europe Daylight Time)"),
                            deliverer: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                            owner: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                            size: 'Box',
                            fromCity: 'Berlin',
                            toCity: 'Berlin',
                            fromLocation: 'ordering.dragons.grownup',
                            toLocation: 'chopper.coaster.reclaim',
                            receiver: '0xB266d94519C25fa0b6deAEebF074298051560Ae9',
                            currentState: 'Delivery Pending',
                            orderId: '0xc55365145A14f719a0D7BceB59d02020098eb982',
                            highestBid: 'X'
                        });

                        $rootScope.contractList.push({
                            closeTimestampAuction: Date.now(),
                            deliveryTimestamp: Date.now(),
                            bids: {
                                value: 4,
                                bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                timestamp: Date.now()
                            },
                            dueTimestampDelivery: Date.now(),
                            pickupTimestamp: new Date("Wed Nov 15 2017 12:31:15 GMT+0200 (W. Europe Daylight Time)"),
                            deliverer: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                            owner: '0xB266d94519C25fa0b6deAEebF074298051560Ae9',
                            size: 'Box',
                            fromCity: 'Berlin',
                            toCity: 'Berlin',
                            fromLocation: 'watch.pavement.rent',
                            toLocation: 'other.landmark.lion',
                            receiver: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                            currentState: 'Delivery Pending',
                            orderId: '0xE002f7102912fCCA12871D7745789E5eb9d65E58',
                            highestBid: 4
                        });

                        $rootScope.contractList.push({
                            closeTimestampAuction: Date.now(),
                            deliveryTimestamp: Date.now(),
                            bids: {
                                value: 45,
                                bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                timestamp: Date.now()
                            },
                            dueTimestampDelivery: Date.now(),
                            pickupTimestamp: new Date("Wed Nov 15 2017 14:55:26 GMT+0200 (W. Europe Daylight Time)"),
                            deliverer: '0xB266d94519C25fa0b6deAEebF074298051560Ae9',
                            owner: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                            size: 'Box',
                            fromCity: 'Berlin',
                            toCity: 'Frankfurt',
                            fromLocation: 'diagram.ended.tingled',
                            toLocation: 'addicted.bucket.hammer',
                            receiver: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                            currentState: 'Delivery Pending',
                            orderId: '0x5e1f0FB612ccf6f4d9FED70292c29C4f28c0c432',
                            highestBid: 45
                        });

                        $rootScope.contractList.push({
                            closeTimestampAuction: Date.now(),
                            deliveryTimestamp: Date.now(),
                            bids: {
                                value: 10,
                                bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                timestamp: Date.now()
                            },
                            dueTimestampDelivery: Date.now(),
                            pickupTimestamp: new Date("Wed Nov 15 2017 16:28:19 GMT+0200 (W. Europe Daylight Time)"),
                            deliverer: '0xB266d94519C25fa0b6deAEebF074298051560Ae9',
                            owner: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                            size: 'Letter',
                            fromCity: 'Hamburg',
                            toCity: 'Munich',
                            fromLocation: 'bucket.green.bed',
                            toLocation: 'pencil.energy.dog',
                            receiver: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                            currentState: 'Delivered',
                            orderId: '0x5e1f0FB612ccf6f4d9FED70292c29C4f28c0c432',
                            highestBid: 10
                        });

                        $rootScope.contractList.push({
                            closeTimestampAuction: Date.now(),
                            deliveryTimestamp: Date.now(),
                            bids: {
                                value: 53,
                                bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                timestamp: Date.now()
                            },
                            dueTimestampDelivery: Date.now(),
                            pickupTimestamp: new Date("Wed Nov 15 2017 17:21:29 GMT+0200 (W. Europe Daylight Time)"),
                            deliverer: '0xZ820d94519C25fa0b6deAEebF074298051560Ae9',
                            owner: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                            size: 'Palette',
                            fromCity: 'Rostock',
                            toCity: 'Berlin',
                            fromLocation: 'water.chair.fork',
                            toLocation: 'cat.shoes.black',
                            receiver: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                            currentState: 'Delivered',
                            orderId: '0x5e1f0FB612ccf6f4d9FED70292c29C4f28c0c432',
                            highestBid: 53
                        });

                        $rootScope.contractList.push({
                            closeTimestampAuction: Date.now(),
                            deliveryTimestamp: Date.now(),
                            bids: {
                                value: 9,
                                bidder: '0xA8C471fFA6FF704c31276976f6669B7A5395BD98',
                                timestamp: Date.now()
                            },
                            dueTimestampDelivery: Date.now(),
                            pickupTimestamp: new Date("Wed Nov 15 2017 19:36:53 GMT+0200 (W. Europe Daylight Time)"),
                            deliverer: '0xB266d94519C25fa0b6deAEebF074298051560Ae9',
                            owner: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                            size: 'Box',
                            fromCity: 'Karlsruhe',
                            toCity: 'Jena',
                            fromLocation: 'bucket.hammer.tingled',
                            toLocation: 'addicted.ended.diagram',
                            receiver: '0x4F674CEC541004d1786F4BbB0A12e9AAA48e48ee',
                            currentState: 'Delivered',
                            orderId: '0x5e1f0FB612ccf6f4d9FED70292c29C4f28c0c432',
                            highestBid: 9
                        });
                    }

                    $rootScope.contractRequestSenderName = "Pluto Tech";
                    $rootScope.contractRequestSenderAddress = "ordering.dragons.grownup";

                    $rootScope.contractRequestRecipientName = "Rafael Schmidt";
                    $rootScope.contractRequestRecipientAddress = "addicted.bucket.hammer";

                    $rootScope.contractRequestDueDelivery = "2017-11-19";
                    $rootScope.contractRequestWeight = "40";
                    $rootScope.contractRequestVolume = "1.7";

                    $rootScope.showEstimations = false;
                    $rootScope.showOrderForm = true;
                    $rootScope.showOrderSummary = false;

                });

            function getChainInfos() {
                var deferred = $q.defer();
                deferred.resolve(0);
                return deferred.promise;
            }
        };

        $scope.calculateEstimations = function() {
            $rootScope.showEstimations = true;
        };

        $scope.goToDashboard = function() {
            $location.path('/dashboard');
        };

        $scope.deployContract = function() {

            $rootScope.showOrderForm = false;
            $rootScope.showOrderSummary = true;

            var contractRequestRecipient = $scope.contractRequestRecipient;
            var contractRequestFromAddress = $scope.contractRequestFromAddress;
            var contractRequestToAddress = $scope.contractRequestToAddress;
            var contractRequestDueDelivery = $scope.contractRequestDueDelivery;
            var contractRequestPackageSize = $scope.contractRequestPackageSize;

            $rootScope.contractList.push({
                closeTimestampAuction: '',
                deliveryTimestamp: '',
                bids: {
                },
                dueTimestampDelivery: '',
                pickupTimestamp: '',
                deliverer: '',
                owner: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                size: contractRequestPackageSize,
                fromCity: 'Bonn',
                toCity: 'Bonn',
                fromLocation: contractRequestFromAddress,
                toLocation: contractRequestToAddress,
                receiver: contractRequestRecipient,
                currentState: 'OPEN',
                orderId: data,
                highestBid: -1
            });


            /*
            $.post('https://localhost/deliveries', {
                "fromLocation":contractRequestFromAddress,
                "toLocation":contractRequestToAddress,
                "size":contractRequestPackageSize,
                "dueDeliveryTimestamp":contractRequestDueDelivery,
                "receiverAddress":contractRequestRecipient,
                "senderAddress":"0x6e4f388f747225bc8d37d9c5555e81bf76a5299d",
                "password":"OmniUber2017!?"
            })
            .done(function(data) {
                console.log(data);

                if (data!==undefined){
                    $rootScope.contractList.push({
                        closeTimestampAuction: '',
                        deliveryTimestamp: '',
                        bids: {
                        },
                        dueTimestampDelivery: '',
                        pickupTimestamp: '',
                        deliverer: '',
                        owner: '0x6e4F388f747225bc8d37D9C5555e81BF76A5299d',
                        size: contractRequestPackageSize,
                        fromCity: 'Berlin',
                        toCity: 'Berlin',
                        fromLocation: contractRequestFromAddress,
                        toLocation: contractRequestToAddress,
                        receiver: contractRequestRecipient,
                        currentState: 'Auction Running',
                        orderId: data,
                        highestBid: -1
                    });

                    $location.path('/dashboard');
                }
            });*/
        };

        $scope.init();
        console.log($scope.result);
});
